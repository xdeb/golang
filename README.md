## golang

[Go programming language](https://golang.org) compiler, linker, compiled stdlib.

The Go programming language is an open source project to make programmers more
productive. Go is expressive, concise, clean, and efficient. Its concurrency
mechanisms make it easy to write programs that get the most out of multicore
and networked machines, while its novel type system enables flexible and
modular program construction. Go compiles quickly to machine code yet has the
convenience of garbage collection and the power of run-time reflection. It's a
fast, statically typed, compiled language that feels like a dynamically typed,
interpreted language.

This package provides an assembler, compiler, linker, and compiled libraries
for the Go programming language.

Go supports cross-compilation, but as of Go 1.5, it is no longer necessary to
pre-compile the standard library inside GOROOT for cross-compilation to work.

This repository provides the latest Debian/Ubuntu package.

## Usage (APT installation)

1. Set up the APT key

    Add the repository's APT Key by following command:

        wget -O /etc/apt/trusted.gpg.d/xdeb-gitlab-io.gpg https://xdeb.gitlab.io/golang/pubkey.gpg

    or (if you prefer text formatted key file)

        wget -O /etc/apt/trusted.gpg.d/xdeb-gitlab-io.asc https://xdeb.gitlab.io/golang/pubkey.asc

2. Set up APT sources

    Create a new source list file at `/etc/apt/sources.list.d/xdeb-gitlab-io.list` with the
    content as following (debian/bookworm for example):

        deb https://xdeb.gitlab.io/golang/debian bookworm contrib

    The available source list files are:

    * Debian 11 (bullseye)

        deb https://xdeb.gitlab.io/golang/debian bullseye contrib

    * Debian 12 (bookworm)

        deb https://xdeb.gitlab.io/golang/debian bookworm contrib

    * Ubuntu 20.04 TLS (Focal Fossa)

        deb https://xdeb.gitlab.io/golang/ubuntu focal universe

    * Ubuntu 22.04 TLS (Jammy Jellyfish)

        deb https://xdeb.gitlab.io/golang/ubuntu jammy universe

    * Ubuntu 24.04 (Noble Numbat)

        deb https://xdeb.gitlab.io/golang/ubuntu noble universe

3. Install `golang`

        apt-get update && apt-get install golang
